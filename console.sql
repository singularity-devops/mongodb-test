db.users.find().pretty()

db.users.find({"age": 23})

db.users.find({$where: "this.age>20"})

db.users.deleteOne({$where: "this.age>24"})

db.users.findOneAndDelete({_id: ObjectId('629d90302366d551c79bab43')})

db.users.find({"languages": "Turkish"})

db.users.insertOne({
"name": "Max",
"age": 33, "languages": ["Kazakh", "Russian", "English", "Turkish"],
"nationality": "Kazakh",
"interests": ["movie", "music", "books"]
});

db.users.insertMany([{
    "name": "Alice",
    "age": 23
},
{
    "name": "Bob",
    "age": 23,
    "job": "Software Engineer"
}]);

db.students.insertOne({
    "firstname": "Makhambet",
    "lastname": "Torezhan",
    "email": 'makhambet.torezhan@nu.edu.kz',
    "age": 24,
    "dob": "1998-05-26",
    "gender": "male",
    "nationality": "Kazakh",
    "school": "SEDS",
    "major": "Computer Science",
    "year_of_study": 4,
    "advisor": "Selim Temizer",
    "courses": ['Operating Systems', 'Open-Source Software', 'Software Engineering', 'Political Science', 'Critical Thinking']
})

db.students.insertOne({
    "firstname": "Alice",
    "lastname": "Jackson",
    "email": 'alice.jackson@nu.edu.kz',
    "age": 26,
    "dob": "1996-05-26",
    "gender": "female",
    "nationality": "US American",
    "school": "SSH",
    "major": "History",
    "year_of_study": 4,
    "advisor": "Philip Enns",
    "courses": ['History of Kazakhstan', 'Soviet History', 'Modern History of Kazakhstan', 'Political Science']
})

db.students.insertOne({
    "firstname": "Bob",
    "lastname": "Swagger",
    "email": 'bob.swagger@nu.edu.kz',
    "age": 36,
    "dob": "1986-05-26",
    "gender": "male",
    "nationality": "US American",
    "school": "SMG",
    "major": "Petroleum",
    "year_of_study": 2,
    "advisor": "Will Anderson",
    "courses": ['History of Kazakhstan', 'Soviet History']
})

db.students.insertOne({
    "firstname": "Arman",
    "lastname": "Suleimenov",
    "email": 'arman.suleimenov@nu.edu.kz',
    "age": 36,
    "dob": "1986-05-26",
    "gender": "male",
    "nationality": "Kazakh",
    "school": "SSH",
    "major": "Linguistics",
    "year_of_study": 2
})

db.students.insertOne({
    "firstname": "Azat",
    "lastname": "Zhenges",
    "email": 'azat.zhenges@nu.edu.kz',
    "age": 26,
    "dob": "1996-05-26",
    "gender": "male",
    "nationality": "Kazakh",
    "year_of_study": 2
})

db.students.insertOne({
    "firstname": "Azamat",
    "lastname": "Aitugan",
    "email": 'azamat.aitugan@nu.edu.kz',
    "age": 27,
    "dob": "1995-05-26",
    "gender": "male",
    "nationality": "Kazakh",
    "year_of_study": 2
})

db.students.insertOne({
    "firstname": "Maksat",
    "lastname": "Smagulov",
    "email": 'maksat.smagulov@nu.edu.kz',
    "age": 36,
    "dob": "1986-05-26",
    "gender": "male",
    "nationality": "Kazakh",
    "school": "SEDS",
    "major": "Robotics",
    "year_of_study": 2,
    "courses": ['Image Processing', 'Introduction to Circuits']
})

db.students.insertOne({
    "firstname": "Sam",
    "lastname": "Gulli",
    "email": 'sam.gulli@nu.edu.kz',
    "age": 26,
    "dob": "1996-05-26",
    "gender": "female",
    "nationality": "US American",
    "school": "SEDS",
    "major": "Robotics",
    "year_of_study": 2,
})

db.students.insertOne({
    "firstname": "Maksut",
    "lastname": "Kusainov",
    "email": 'maksut.kusainov@nu.edu.kz',
    "age": 27,
    "dob": "1995-05-26",
    "gender": "male",
    "nationality": "Kazakh"
})

db.students.insertOne({
    "firstname": "Ibrahim",
    "lastname": "Zaharov",
    "age": 27,
    "dob": "1995-05-26",
    "gender": "male",
    "nationality": "Russian"
})
db.students.find()
db.students.find({age: {$gt: 26}})
db.students.find({age: {$gt: 27}})
db.students.find({courses: {$in: ['History of Kazakhstan']}})
db.students.find({courses: {$nin: ['History of Kazakhstan']}})
db.students.find({age: {$ne: 26}})
db.students.find({age: {$lt: 26}})
db.students.find({$or: [{age: 25}, {school: 'SEDS'}]})

// select kazakh men or women from ssh

db.students.find(
{
    $or: [
        {
            $and:
                [
                    {nationality: "Kazakh"},
                    {gender: "male"}
                ]
        } ,
        {
            $and:
                [
                    {school: "SSH"} ,
                    {gender: "female"}
                ]
        }
    ]
 })

db.students.updateOne(
    {firstname: 'Makhambet'},
    {
        $set: {age: 30}
    }
)

db.students.replaceOne({lastname: 'Zaharov'},
    {
        lastname: 'Makarov', nationality: 'Tatarin', school: 'SEDS'
    }
)

db.students.updateOne({firstname: 'Makhambet'}, {$unset: {year_of_study: 4}})

db.students.updateOne({firstname: 'Makhambet'}, {$push: {courses: 'Computer Organization'}})

db.students.updateOne({firstname: 'Makhambet'}, {$push: {courses: {$each: ['Formal Languages', 'Database Systems', 'Introduction to Robotics']}}})

db.students.findOne({firstname: 'Makhambet'})